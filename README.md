# Call Service

The notify service is used to push live events to a user.

## Documentation

Test: https://connect-test.kominal.com/notification-service/api-docs
Production: https://connect.kominal.com/notification-service/api-docs
