import Service from '@kominal/service-util/helper/service';
import { SMQHandler } from './handler.smq';
import subscription from './routes/subscription';

const service = new Service({
	id: 'notification-service',
	name: 'Notification Service',
	description: 'Enables other services to show live notifications to users.',
	jsonLimit: '16mb',
	routes: [subscription],
	database: true,
	squad: true,
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
