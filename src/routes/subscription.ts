import Router from '@kominal/service-util/helper/router';
import SubscriptionDatabase from '@kominal/connect-models/subscription/subscription.database';
import { verifyParameter } from '@kominal/service-util/helper/util';

const router = new Router();

/**
 * Lists all active push subscriptions
 * @group Public
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - A list of push subscriptions
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/', async (req, res, userId) => {
	const subscriptions = (await SubscriptionDatabase.find({ userId })).map((subscription) => {
		return {
			id: subscription.id,
			device: {
				data: subscription.get('device'),
				iv: subscription.get('deviceIv'),
			},
			created: subscription.get('created'),
		};
	});
	res.status(200).send(subscriptions);
});

/**
 * Registers a new push subscription
 * @group Protected
 * @security JWT
 * @route POST /
 * @consumes application/json
 * @produces application/json
 * @param {string} pushSubscription.body.required - The pushSubscription object
 * @param {string} device.body.required - The device identifier encrypted with the master encryption key
 * @returns {void} 200 - The push subscription was added
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const body = req.body;
	verifyParameter(body.pushSubscription, body.device);
	const pushSubscription = JSON.stringify(body.pushSubscription);
	if ((await SubscriptionDatabase.findOne({ userId, pushSubscription })) != null) {
		res.status(200).send();
		return;
	}
	await SubscriptionDatabase.create({
		userId,
		pushSubscription,
		device: body.device.data,
		deviceIv: body.device.iv,
		created: new Date(),
	});
	res.status(200).send();
});

/**
 * Removes an existing push subscription
 * @group Protected
 * @security JWT
 * @route DELETE /{id}
 * @consumes application/json
 * @produces application/json
 * @param {string} id.path.required - The id of the push subscription
 * @returns {void} 200 - The push subscription was deleted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/:id', async (req, res, userId) => {
	verifyParameter(req.params.id);
	await SubscriptionDatabase.deleteMany({ _id: req.params.id, userId });
	res.status(200).send();
});

export default router.getExpressRouter();
