import webpush from 'web-push';
import { Packet } from '@kominal/service-util/model/packet';
import service from '.';
import { info, error, debug } from '@kominal/service-util/helper/log';
import SocketConnectionDatabase from '@kominal/connect-models/socketconnection/socketconnection.database';
import { VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY } from '@kominal/service-util/helper/environment';
import SubscriptionDatabase from '@kominal/connect-models/subscription/subscription.database';

webpush.setVapidDetails('mailto:mail@connect.kominal.com', VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		const smqClient = service.getSMQClient();
		if (smqClient) {
			smqClient.subscribe('QUEUE', 'NOTIFICATION.SYNC');
			smqClient.subscribe('QUEUE', 'NOTIFICATION.CONNECT');
			smqClient.subscribe('QUEUE', 'NOTIFICATION.DISCONNECT');
			smqClient.subscribe('QUEUE', 'INTERNAL.NOTIFICATION');
		}
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		const { destinationType, destinationName, event } = packet;
		if (!event) {
			return;
		}

		const { type, body } = event;
		if (destinationType === 'QUEUE' && destinationName === 'NOTIFICATION.SYNC') {
			this.onSync(type, body);
		} else if (destinationType === 'QUEUE' && destinationName === 'NOTIFICATION.CONNECT') {
			this.onUserConnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'NOTIFICATION.DISCONNECT') {
			this.onUserDisconnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'INTERNAL.NOTIFICATION') {
			this.onNotification(event);
		}
	}

	async onSync(type: string, body: any) {
		if (type === 'SYNC') {
			const { taskIds } = body;
			info(`Starting sync for the following socket services: ${taskIds}`);
			const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
			for (const socketConnection of socketConnections) {
				this.onUserDisconnect({
					taskId: socketConnection.get('taskId'),
					connectionId: socketConnection.get('connectionId'),
					userId: socketConnection.get('userId'),
				});
			}
			const result = await SocketConnectionDatabase.deleteMany({ taskId: { $not: { $in: taskIds } } });
			if (result.deletedCount && result.deletedCount > 0) {
				info(`Removed ${result.deletedCount} dead socket connections!`);
			}
		} else if (type === 'RESET') {
			const taskId: string = body.taskId;
			info(`Starting reset for the following socket service: ${taskId}`);
			const connections: { connectionId: string; userId: string }[] = body.connections;
			const connectionIds = connections.map((c) => c.connectionId);
			const socketConnections = await SocketConnectionDatabase.find({ taskId, connectionId: { $not: { $in: connectionIds } } });
			for (const socketConnection of socketConnections) {
				this.onUserDisconnect({ taskId, connectionId: socketConnection.get('connectionId'), userId: socketConnection.get('userId') });
			}

			for (const connection of connections) {
				const { connectionId, userId } = connection;
				if ((await SocketConnectionDatabase.countDocuments({ taskId, connectionId, userId })) === 0) {
					this.onUserConnect({ taskId, connectionId, userId });
				}
			}
		}
	}
	async onUserConnect(body: { taskId: string; connectionId: string; userId: string }) {
		const { taskId, connectionId, userId } = body;

		await SocketConnectionDatabase.findOneAndUpdate({ connectionId, taskId }, { userId }, { upsert: true });

		debug(`Connection from user ${userId}, connection ${connectionId} established.`);
	}

	async onUserDisconnect(body: { taskId: string; connectionId: string; userId: string }) {
		const { taskId, connectionId, userId } = body;

		await SocketConnectionDatabase.deleteMany({ taskId, connectionId });

		debug(`Connection from user ${userId}, connection ${connectionId} lost.`);
	}

	async onNotification(event: { type?: string; body?: any }) {
		const { type, body } = event;
		if (!type && !body) {
			return;
		}
		if (type === 'MESSAGE.NEW') {
			const { userIds, groupId, messageId } = body;

			const notificationPayload = JSON.stringify({
				notification: {
					title: 'Connect',
					body: 'You have a new message!',
					icon: 'assets/images/logo/logo-512x512.png',
					vibrate: [100, 50, 100],
					data: { url: `/connect/group/${groupId}` },
					actions: [{ action: 'open-chat', title: 'Open chat' }],
					renotify: true,
					tag: `MESSAGE_NEW_${groupId}`,
				},
			});

			for (const userId of userIds) {
				if ((await SocketConnectionDatabase.countDocuments({ userId })) > 0) {
					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'MESSAGE.NEW', { groupId, messageId });
				} else {
					const subscriptions = await SubscriptionDatabase.find({ userId });
					for (const subscription of subscriptions) {
						try {
							const result = await webpush.sendNotification(JSON.parse(subscription.get('pushSubscription')), notificationPayload);
							if (result.statusCode > 299) {
								await SubscriptionDatabase.remove(subscription);
							}
						} catch (error) {
							console.log(error);
						}
					}
				}
			}
		}
	}
}
